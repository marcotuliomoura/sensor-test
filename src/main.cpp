#include <Arduino.h>
#include <DHT_U.h>

#define DHTPIN 4
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

float temperatura;
float umidade;

// put your setup code here, to run once:
void setup() {
  Serial.begin(115200);
  Serial.println("Inicio setup DTH");
  delay(2000);
  dht.begin();
  
}

void loop() {
  delay(10000);
  umidade = dht.readHumidity();
    // Read temperature as Celsius (the default)
  temperatura = dht.readTemperature();
  
  Serial.println("=================");
  Serial.println("Umidade: ");
  Serial.println(umidade);
  Serial.println("Temperatura: ");
  Serial.println(temperatura);
  Serial.println("=================");
}